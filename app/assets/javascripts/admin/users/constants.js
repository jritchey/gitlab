export const USER_AVATAR_SIZE = 32;

export const SHORT_DATE_FORMAT = 'd mmm, yyyy';
